﻿namespace MobiFlight.Panels
{
    partial class DisplayLedDisplayPanel
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.displayLedDigit7CheckBox = new System.Windows.Forms.CheckBox();
            this.ledDisplayComboBoxLabel = new System.Windows.Forms.Label();
            this.displayLedDigit6CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigit5CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigit4CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint7CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigit3CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigit2CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint6CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint5CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPointFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.displayLedDecimalPoint4CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint3CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint2CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint1CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDecimalPoint0CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigit1CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigit0CheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDigitFlowLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.displayLedModuleSizeComboBox = new System.Windows.Forms.ComboBox();
            this.NumberOfDigitsLabel = new System.Windows.Forms.Label();
            this.displayLedPaddingCheckBox = new System.Windows.Forms.CheckBox();
            this.displayLedDisplayLabel0 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel1 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel2 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel3 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel4 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel5 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel6 = new System.Windows.Forms.Label();
            this.displayLedDisplayLabel7 = new System.Windows.Forms.Label();
            this.displayLedDecimalPointLabel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.displayLedAddressComboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.displayLedConnectorComboBox = new System.Windows.Forms.ComboBox();
            this.PaddingCharComboBox = new System.Windows.Forms.ComboBox();
            this.displayLedDecimalPointFlowLayoutPanel.SuspendLayout();
            this.displayLedDigitFlowLayoutPanel.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // displayLedDigit7CheckBox
            // 
            this.displayLedDigit7CheckBox.AutoSize = true;
            this.displayLedDigit7CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit7CheckBox.Location = new System.Drawing.Point(3, 3);
            this.displayLedDigit7CheckBox.Name = "displayLedDigit7CheckBox";
            this.displayLedDigit7CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit7CheckBox.TabIndex = 18;
            this.displayLedDigit7CheckBox.UseVisualStyleBackColor = true;
            // 
            // ledDisplayComboBoxLabel
            // 
            this.ledDisplayComboBoxLabel.AutoSize = true;
            this.ledDisplayComboBoxLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.ledDisplayComboBoxLabel.Location = new System.Drawing.Point(13, 8);
            this.ledDisplayComboBoxLabel.Name = "ledDisplayComboBoxLabel";
            this.ledDisplayComboBoxLabel.Size = new System.Drawing.Size(89, 13);
            this.ledDisplayComboBoxLabel.TabIndex = 53;
            this.ledDisplayComboBoxLabel.Text = "Addr / Connector";
            // 
            // displayLedDigit6CheckBox
            // 
            this.displayLedDigit6CheckBox.AutoSize = true;
            this.displayLedDigit6CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit6CheckBox.Location = new System.Drawing.Point(24, 3);
            this.displayLedDigit6CheckBox.Name = "displayLedDigit6CheckBox";
            this.displayLedDigit6CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit6CheckBox.TabIndex = 19;
            this.displayLedDigit6CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigit5CheckBox
            // 
            this.displayLedDigit5CheckBox.AutoSize = true;
            this.displayLedDigit5CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit5CheckBox.Location = new System.Drawing.Point(45, 3);
            this.displayLedDigit5CheckBox.Name = "displayLedDigit5CheckBox";
            this.displayLedDigit5CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit5CheckBox.TabIndex = 20;
            this.displayLedDigit5CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigit4CheckBox
            // 
            this.displayLedDigit4CheckBox.AutoSize = true;
            this.displayLedDigit4CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit4CheckBox.Location = new System.Drawing.Point(66, 3);
            this.displayLedDigit4CheckBox.Name = "displayLedDigit4CheckBox";
            this.displayLedDigit4CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit4CheckBox.TabIndex = 21;
            this.displayLedDigit4CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint7CheckBox
            // 
            this.displayLedDecimalPoint7CheckBox.AutoSize = true;
            this.displayLedDecimalPoint7CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint7CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint7CheckBox.Location = new System.Drawing.Point(3, 3);
            this.displayLedDecimalPoint7CheckBox.Name = "displayLedDecimalPoint7CheckBox";
            this.displayLedDecimalPoint7CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint7CheckBox.TabIndex = 44;
            this.displayLedDecimalPoint7CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigit3CheckBox
            // 
            this.displayLedDigit3CheckBox.AutoSize = true;
            this.displayLedDigit3CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit3CheckBox.Location = new System.Drawing.Point(87, 3);
            this.displayLedDigit3CheckBox.Name = "displayLedDigit3CheckBox";
            this.displayLedDigit3CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit3CheckBox.TabIndex = 22;
            this.displayLedDigit3CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigit2CheckBox
            // 
            this.displayLedDigit2CheckBox.AutoSize = true;
            this.displayLedDigit2CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit2CheckBox.Location = new System.Drawing.Point(108, 3);
            this.displayLedDigit2CheckBox.Name = "displayLedDigit2CheckBox";
            this.displayLedDigit2CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit2CheckBox.TabIndex = 23;
            this.displayLedDigit2CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint6CheckBox
            // 
            this.displayLedDecimalPoint6CheckBox.AutoSize = true;
            this.displayLedDecimalPoint6CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint6CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint6CheckBox.Location = new System.Drawing.Point(24, 3);
            this.displayLedDecimalPoint6CheckBox.Name = "displayLedDecimalPoint6CheckBox";
            this.displayLedDecimalPoint6CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint6CheckBox.TabIndex = 45;
            this.displayLedDecimalPoint6CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint5CheckBox
            // 
            this.displayLedDecimalPoint5CheckBox.AutoSize = true;
            this.displayLedDecimalPoint5CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint5CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint5CheckBox.Location = new System.Drawing.Point(45, 3);
            this.displayLedDecimalPoint5CheckBox.Name = "displayLedDecimalPoint5CheckBox";
            this.displayLedDecimalPoint5CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint5CheckBox.TabIndex = 46;
            this.displayLedDecimalPoint5CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPointFlowLayoutPanel
            // 
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint7CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint6CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint5CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint4CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint3CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint2CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint1CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Controls.Add(this.displayLedDecimalPoint0CheckBox);
            this.displayLedDecimalPointFlowLayoutPanel.Location = new System.Drawing.Point(105, 85);
            this.displayLedDecimalPointFlowLayoutPanel.Name = "displayLedDecimalPointFlowLayoutPanel";
            this.displayLedDecimalPointFlowLayoutPanel.Size = new System.Drawing.Size(200, 22);
            this.displayLedDecimalPointFlowLayoutPanel.TabIndex = 65;
            this.displayLedDecimalPointFlowLayoutPanel.WrapContents = false;
            // 
            // displayLedDecimalPoint4CheckBox
            // 
            this.displayLedDecimalPoint4CheckBox.AutoSize = true;
            this.displayLedDecimalPoint4CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint4CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint4CheckBox.Location = new System.Drawing.Point(66, 3);
            this.displayLedDecimalPoint4CheckBox.Name = "displayLedDecimalPoint4CheckBox";
            this.displayLedDecimalPoint4CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint4CheckBox.TabIndex = 47;
            this.displayLedDecimalPoint4CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint3CheckBox
            // 
            this.displayLedDecimalPoint3CheckBox.AutoSize = true;
            this.displayLedDecimalPoint3CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint3CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint3CheckBox.Location = new System.Drawing.Point(87, 3);
            this.displayLedDecimalPoint3CheckBox.Name = "displayLedDecimalPoint3CheckBox";
            this.displayLedDecimalPoint3CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint3CheckBox.TabIndex = 48;
            this.displayLedDecimalPoint3CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint2CheckBox
            // 
            this.displayLedDecimalPoint2CheckBox.AutoSize = true;
            this.displayLedDecimalPoint2CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint2CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint2CheckBox.Location = new System.Drawing.Point(108, 3);
            this.displayLedDecimalPoint2CheckBox.Name = "displayLedDecimalPoint2CheckBox";
            this.displayLedDecimalPoint2CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint2CheckBox.TabIndex = 49;
            this.displayLedDecimalPoint2CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint1CheckBox
            // 
            this.displayLedDecimalPoint1CheckBox.AutoSize = true;
            this.displayLedDecimalPoint1CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint1CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint1CheckBox.Location = new System.Drawing.Point(129, 3);
            this.displayLedDecimalPoint1CheckBox.Name = "displayLedDecimalPoint1CheckBox";
            this.displayLedDecimalPoint1CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint1CheckBox.TabIndex = 50;
            this.displayLedDecimalPoint1CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDecimalPoint0CheckBox
            // 
            this.displayLedDecimalPoint0CheckBox.AutoSize = true;
            this.displayLedDecimalPoint0CheckBox.Dock = System.Windows.Forms.DockStyle.Left;
            this.displayLedDecimalPoint0CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPoint0CheckBox.Location = new System.Drawing.Point(150, 3);
            this.displayLedDecimalPoint0CheckBox.Name = "displayLedDecimalPoint0CheckBox";
            this.displayLedDecimalPoint0CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDecimalPoint0CheckBox.TabIndex = 51;
            this.displayLedDecimalPoint0CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigit1CheckBox
            // 
            this.displayLedDigit1CheckBox.AutoSize = true;
            this.displayLedDigit1CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit1CheckBox.Location = new System.Drawing.Point(129, 3);
            this.displayLedDigit1CheckBox.Name = "displayLedDigit1CheckBox";
            this.displayLedDigit1CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit1CheckBox.TabIndex = 24;
            this.displayLedDigit1CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigit0CheckBox
            // 
            this.displayLedDigit0CheckBox.AutoSize = true;
            this.displayLedDigit0CheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDigit0CheckBox.Location = new System.Drawing.Point(150, 3);
            this.displayLedDigit0CheckBox.Name = "displayLedDigit0CheckBox";
            this.displayLedDigit0CheckBox.Size = new System.Drawing.Size(15, 14);
            this.displayLedDigit0CheckBox.TabIndex = 25;
            this.displayLedDigit0CheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDigitFlowLayoutPanel
            // 
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit7CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit6CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit5CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit4CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit3CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit2CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit1CheckBox);
            this.displayLedDigitFlowLayoutPanel.Controls.Add(this.displayLedDigit0CheckBox);
            this.displayLedDigitFlowLayoutPanel.Location = new System.Drawing.Point(106, 55);
            this.displayLedDigitFlowLayoutPanel.Name = "displayLedDigitFlowLayoutPanel";
            this.displayLedDigitFlowLayoutPanel.Size = new System.Drawing.Size(200, 18);
            this.displayLedDigitFlowLayoutPanel.TabIndex = 68;
            // 
            // displayLedModuleSizeComboBox
            // 
            this.displayLedModuleSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.displayLedModuleSizeComboBox.FormattingEnabled = true;
            this.displayLedModuleSizeComboBox.Items.AddRange(new object[] {
            "3",
            "4",
            "5",
            "6",
            "8"});
            this.displayLedModuleSizeComboBox.Location = new System.Drawing.Point(107, 30);
            this.displayLedModuleSizeComboBox.Name = "displayLedModuleSizeComboBox";
            this.displayLedModuleSizeComboBox.Size = new System.Drawing.Size(35, 21);
            this.displayLedModuleSizeComboBox.TabIndex = 67;
            this.displayLedModuleSizeComboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // NumberOfDigitsLabel
            // 
            this.NumberOfDigitsLabel.AutoSize = true;
            this.NumberOfDigitsLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.NumberOfDigitsLabel.Location = new System.Drawing.Point(17, 34);
            this.NumberOfDigitsLabel.Name = "NumberOfDigitsLabel";
            this.NumberOfDigitsLabel.Size = new System.Drawing.Size(83, 13);
            this.NumberOfDigitsLabel.TabIndex = 66;
            this.NumberOfDigitsLabel.Text = "Number of digits";
            // 
            // displayLedPaddingCheckBox
            // 
            this.displayLedPaddingCheckBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedPaddingCheckBox.Location = new System.Drawing.Point(159, 33);
            this.displayLedPaddingCheckBox.Name = "displayLedPaddingCheckBox";
            this.displayLedPaddingCheckBox.Size = new System.Drawing.Size(108, 17);
            this.displayLedPaddingCheckBox.TabIndex = 51;
            this.displayLedPaddingCheckBox.Text = "Use Left Padding";
            this.displayLedPaddingCheckBox.UseVisualStyleBackColor = true;
            // 
            // displayLedDisplayLabel0
            // 
            this.displayLedDisplayLabel0.AutoSize = true;
            this.displayLedDisplayLabel0.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel0.Location = new System.Drawing.Point(108, 73);
            this.displayLedDisplayLabel0.Name = "displayLedDisplayLabel0";
            this.displayLedDisplayLabel0.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel0.TabIndex = 56;
            this.displayLedDisplayLabel0.Text = "1";
            // 
            // displayLedDisplayLabel1
            // 
            this.displayLedDisplayLabel1.AutoSize = true;
            this.displayLedDisplayLabel1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel1.Location = new System.Drawing.Point(129, 73);
            this.displayLedDisplayLabel1.Name = "displayLedDisplayLabel1";
            this.displayLedDisplayLabel1.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel1.TabIndex = 52;
            this.displayLedDisplayLabel1.Text = "2";
            // 
            // displayLedDisplayLabel2
            // 
            this.displayLedDisplayLabel2.AutoSize = true;
            this.displayLedDisplayLabel2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel2.Location = new System.Drawing.Point(150, 73);
            this.displayLedDisplayLabel2.Name = "displayLedDisplayLabel2";
            this.displayLedDisplayLabel2.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel2.TabIndex = 57;
            this.displayLedDisplayLabel2.Text = "3";
            // 
            // displayLedDisplayLabel3
            // 
            this.displayLedDisplayLabel3.AutoSize = true;
            this.displayLedDisplayLabel3.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel3.Location = new System.Drawing.Point(171, 73);
            this.displayLedDisplayLabel3.Name = "displayLedDisplayLabel3";
            this.displayLedDisplayLabel3.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel3.TabIndex = 58;
            this.displayLedDisplayLabel3.Text = "4";
            // 
            // displayLedDisplayLabel4
            // 
            this.displayLedDisplayLabel4.AutoSize = true;
            this.displayLedDisplayLabel4.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel4.Location = new System.Drawing.Point(192, 73);
            this.displayLedDisplayLabel4.Name = "displayLedDisplayLabel4";
            this.displayLedDisplayLabel4.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel4.TabIndex = 59;
            this.displayLedDisplayLabel4.Text = "5";
            // 
            // displayLedDisplayLabel5
            // 
            this.displayLedDisplayLabel5.AutoSize = true;
            this.displayLedDisplayLabel5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel5.Location = new System.Drawing.Point(213, 73);
            this.displayLedDisplayLabel5.Name = "displayLedDisplayLabel5";
            this.displayLedDisplayLabel5.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel5.TabIndex = 60;
            this.displayLedDisplayLabel5.Text = "6";
            // 
            // displayLedDisplayLabel6
            // 
            this.displayLedDisplayLabel6.AutoSize = true;
            this.displayLedDisplayLabel6.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel6.Location = new System.Drawing.Point(234, 73);
            this.displayLedDisplayLabel6.Name = "displayLedDisplayLabel6";
            this.displayLedDisplayLabel6.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel6.TabIndex = 61;
            this.displayLedDisplayLabel6.Text = "7";
            // 
            // displayLedDisplayLabel7
            // 
            this.displayLedDisplayLabel7.AutoSize = true;
            this.displayLedDisplayLabel7.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDisplayLabel7.Location = new System.Drawing.Point(255, 73);
            this.displayLedDisplayLabel7.Name = "displayLedDisplayLabel7";
            this.displayLedDisplayLabel7.Size = new System.Drawing.Size(13, 13);
            this.displayLedDisplayLabel7.TabIndex = 62;
            this.displayLedDisplayLabel7.Text = "8";
            // 
            // displayLedDecimalPointLabel
            // 
            this.displayLedDecimalPointLabel.AutoSize = true;
            this.displayLedDecimalPointLabel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.displayLedDecimalPointLabel.Location = new System.Drawing.Point(16, 86);
            this.displayLedDecimalPointLabel.Name = "displayLedDecimalPointLabel";
            this.displayLedDecimalPointLabel.Size = new System.Drawing.Size(86, 13);
            this.displayLedDecimalPointLabel.TabIndex = 55;
            this.displayLedDecimalPointLabel.Text = "set decimal point";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label5.Location = new System.Drawing.Point(43, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 54;
            this.label5.Text = "use display";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.displayLedAddressComboBox);
            this.flowLayoutPanel1.Controls.Add(this.label9);
            this.flowLayoutPanel1.Controls.Add(this.displayLedConnectorComboBox);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(104, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 27);
            this.flowLayoutPanel1.TabIndex = 69;
            // 
            // displayLedAddressComboBox
            // 
            this.displayLedAddressComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.displayLedAddressComboBox.FormattingEnabled = true;
            this.displayLedAddressComboBox.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F"});
            this.displayLedAddressComboBox.Location = new System.Drawing.Point(3, 3);
            this.displayLedAddressComboBox.MaximumSize = new System.Drawing.Size(135, 0);
            this.displayLedAddressComboBox.MinimumSize = new System.Drawing.Size(35, 0);
            this.displayLedAddressComboBox.Name = "displayLedAddressComboBox";
            this.displayLedAddressComboBox.Size = new System.Drawing.Size(132, 21);
            this.displayLedAddressComboBox.TabIndex = 65;
            // 
            // label9
            // 
            this.label9.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label9.Location = new System.Drawing.Point(141, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 27);
            this.label9.TabIndex = 67;
            this.label9.Text = "/";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // displayLedConnectorComboBox
            // 
            this.displayLedConnectorComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.displayLedConnectorComboBox.FormattingEnabled = true;
            this.displayLedConnectorComboBox.Items.AddRange(new object[] {
            "1",
            "2"});
            this.displayLedConnectorComboBox.Location = new System.Drawing.Point(159, 3);
            this.displayLedConnectorComboBox.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.displayLedConnectorComboBox.Name = "displayLedConnectorComboBox";
            this.displayLedConnectorComboBox.Size = new System.Drawing.Size(38, 21);
            this.displayLedConnectorComboBox.TabIndex = 66;
            // 
            // PaddingCharComboBox
            // 
            this.PaddingCharComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.PaddingCharComboBox.FormattingEnabled = true;
            this.PaddingCharComboBox.Items.AddRange(new object[] {
            "0",
            "Space"});
            this.PaddingCharComboBox.Location = new System.Drawing.Point(266, 30);
            this.PaddingCharComboBox.Name = "PaddingCharComboBox";
            this.PaddingCharComboBox.Size = new System.Drawing.Size(35, 21);
            this.PaddingCharComboBox.TabIndex = 70;
            // 
            // DisplayLedDisplayPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ledDisplayComboBoxLabel);
            this.Controls.Add(this.displayLedDecimalPointFlowLayoutPanel);
            this.Controls.Add(this.displayLedDigitFlowLayoutPanel);
            this.Controls.Add(this.displayLedModuleSizeComboBox);
            this.Controls.Add(this.NumberOfDigitsLabel);
            this.Controls.Add(this.PaddingCharComboBox);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.displayLedPaddingCheckBox);
            this.Controls.Add(this.displayLedDisplayLabel0);
            this.Controls.Add(this.displayLedDisplayLabel1);
            this.Controls.Add(this.displayLedDisplayLabel2);
            this.Controls.Add(this.displayLedDisplayLabel3);
            this.Controls.Add(this.displayLedDisplayLabel4);
            this.Controls.Add(this.displayLedDisplayLabel5);
            this.Controls.Add(this.displayLedDisplayLabel6);
            this.Controls.Add(this.displayLedDisplayLabel7);
            this.Controls.Add(this.displayLedDecimalPointLabel);
            this.Controls.Add(this.label5);
            this.Name = "DisplayLedDisplayPanel";
            this.Size = new System.Drawing.Size(318, 111);
            this.displayLedDecimalPointFlowLayoutPanel.ResumeLayout(false);
            this.displayLedDecimalPointFlowLayoutPanel.PerformLayout();
            this.displayLedDigitFlowLayoutPanel.ResumeLayout(false);
            this.displayLedDigitFlowLayoutPanel.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.CheckBox displayLedDigit7CheckBox;
        public System.Windows.Forms.Label ledDisplayComboBoxLabel;
        public System.Windows.Forms.CheckBox displayLedDigit6CheckBox;
        public System.Windows.Forms.CheckBox displayLedDigit5CheckBox;
        public System.Windows.Forms.CheckBox displayLedDigit4CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint7CheckBox;
        public System.Windows.Forms.CheckBox displayLedDigit3CheckBox;
        public System.Windows.Forms.CheckBox displayLedDigit2CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint6CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint5CheckBox;
        public System.Windows.Forms.FlowLayoutPanel displayLedDecimalPointFlowLayoutPanel;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint4CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint3CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint2CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint1CheckBox;
        public System.Windows.Forms.CheckBox displayLedDecimalPoint0CheckBox;
        public System.Windows.Forms.CheckBox displayLedDigit1CheckBox;
        public System.Windows.Forms.CheckBox displayLedDigit0CheckBox;
        public System.Windows.Forms.FlowLayoutPanel displayLedDigitFlowLayoutPanel;
        public System.Windows.Forms.ComboBox displayLedModuleSizeComboBox;
        public System.Windows.Forms.Label NumberOfDigitsLabel;
        public System.Windows.Forms.CheckBox displayLedPaddingCheckBox;
        public System.Windows.Forms.Label displayLedDisplayLabel0;
        public System.Windows.Forms.Label displayLedDisplayLabel1;
        public System.Windows.Forms.Label displayLedDisplayLabel2;
        public System.Windows.Forms.Label displayLedDisplayLabel3;
        public System.Windows.Forms.Label displayLedDisplayLabel4;
        public System.Windows.Forms.Label displayLedDisplayLabel5;
        public System.Windows.Forms.Label displayLedDisplayLabel6;
        public System.Windows.Forms.Label displayLedDisplayLabel7;
        public System.Windows.Forms.Label displayLedDecimalPointLabel;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.ComboBox displayLedAddressComboBox;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.ComboBox displayLedConnectorComboBox;
        public System.Windows.Forms.ComboBox PaddingCharComboBox;
    }
}
